#include "chatclient.cpp"
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    next_token_number = 1;
    ui->setupUi(this);
    addjobs();
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(generate()));
    connect(ui->pushButton, SIGNAL(clicked()), ui->lineEdit, SLOT(clear()));
    connect(ui->actionSet_Server_IP,SIGNAL(triggered()),this, SLOT(changeIP()));
}
void MainWindow::changeIP()
{
    QDialog *getip = new QDialog(this);
    QVBoxLayout *iplayout = new QVBoxLayout();
    ip = new QLineEdit();
    QPushButton *OKButton = new QPushButton("OK");
    iplayout->addWidget(ip);
    iplayout->addWidget(OKButton);
    getip->setLayout(iplayout);
    getip->show();
    connect(OKButton,SIGNAL(clicked()),this, SLOT(updateIP()));
    connect(OKButton, SIGNAL(clicked()), getip, SLOT(accept()));
}
void MainWindow::updateIP()
{
    serv_ip = ip->text();
}

void MainWindow::addjobs()
{
    FILE *fp = fopen("joblist","r");
    if(fp==NULL)
    {
        printf("FILE NOT FOUND ERROR");
        return;
    }
    while(1)
    {
        printf("HERE GOES THE CODE");
        char *s = (char*)malloc(100*sizeof(char));
        fgets(s, 100, fp);
        if(feof(fp))	break;
        QString str(s);
        ui->comboBox->addItem(str);
    }
    return;
}
void MainWindow::generate()
{
    QString message = "tgm\n";
    message = message + QString::number(next_token_number) + "\n";
    message = message + ui->lineEdit->text() + "\n";
    message = message + QString::number(ui->comboBox->currentIndex()) + "\n";
    message = message + "end" + "\n\0";

    QString displayString = "GENERATED TOKEN:\n\nNUMBER:\t"+QString::number(next_token_number) + "\nNAME:\t\t"+ui->lineEdit->text()+"\nTYPE:\t\t"+ui->comboBox->currentText()+"\n\nPlease wait for your turn\n";
    char *reply = (char*)malloc(100*sizeof(char));
    if(communicate(message.toStdString().c_str(),reply, serv_ip.toStdString().c_str()) == EXIT_FAILURE)
    {
        QDialog *disp= new QDialog();
        QVBoxLayout *dialoglayout= new QVBoxLayout();
        QLabel *info = new QLabel("Connection to server failed.");
        connect(disp,SIGNAL(rejected()),this,SLOT(close()));
        dialoglayout->addWidget(info);
        disp->setLayout(dialoglayout);
        disp->show();
    }
    else
    {
        QDialog *disp= new QDialog();
        QVBoxLayout *dialoglayout= new QVBoxLayout();
        QLabel *info = new QLabel(displayString);
        dialoglayout->addWidget(info);
        disp->setLayout(dialoglayout);
        next_token_number++;
        disp->show();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
