#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDialog>
#include <QLineEdit>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

#define PORT 5000
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void chat(char *message);
    void addjobs();
private:
    int next_token_number;
    Ui::MainWindow *ui;
    QLineEdit *ip;
    QString serv_ip;

public slots:
    void generate();
    void changeIP();
    void updateIP();
};

#endif // MAINWINDOW_H
