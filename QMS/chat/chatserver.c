#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
#define PORT 5000
int main(int argc, char *argv[])
{
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr; 

    char sendBuff[1025];
    char recvBuff[1025];
    time_t ticks; 

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(sendBuff, '0', sizeof(sendBuff)); 
    memset(recvBuff, '0', sizeof(recvBuff));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(PORT); 

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 
	
    listen(listenfd, 10); 

    while(1)
    {
        connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 
	printf("Connection established ....Lets chat!\n");
        while(1)
	{
		char *x = malloc(1024*sizeof(char));
		printf("TYPE? ");
		gets(x);
		snprintf(sendBuff, sizeof(sendBuff), "%s\r\n", x);
        	write(connfd, sendBuff, strlen(sendBuff)); 
		sleep(3);
		int err = read(connfd, recvBuff, (sizeof(recvBuff)-1));
		recvBuff[err] = 0;
		fputs(recvBuff, stdout);
	}	
        close(connfd);
        sleep(1);
     }
}
