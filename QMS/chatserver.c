#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#define PORT 5000

int main(int argc, char *argv[])
{
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr; 

    char sendBuff[1025];
    char recvBuff[1025];

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(sendBuff, '0', sizeof(sendBuff)); 
    memset(recvBuff, '0', sizeof(recvBuff));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(PORT); 

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 
	
    listen(listenfd, 10); 
    printf("Server setup complete. Ready to connect.\n");
    while(1)
    {
        connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 
   	int err = read(connfd, recvBuff, (sizeof(recvBuff)-1));
	recvBuff[err] = 0;

	char *token = strtok(message, " \n");
	if(strcmp(token, "tgm")
	{
		if(accept_token(message) == EXIT_FAILURE)
			printf("ERROR IN TOKEN ACCEPTANCE");
		snprintf(sendBuff, sizeof(sendBuff), "ack\nend\n\r");
	}
	else if(strcmp(token, "csm")
	{
		snprintf(sendBuff, sizeof(sendBuff), "%s\r\n", allot_job());
	}
	write(connfd, sendBuff, strlen(sendBuff)); 
	sleep(1);		
        close(connfd);
    }
}
