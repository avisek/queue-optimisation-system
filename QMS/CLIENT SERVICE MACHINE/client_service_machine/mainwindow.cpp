#include "chatclient.cpp"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Token.cpp"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(next_token()));
    connect(ui->actionSet_Server_IP,SIGNAL(triggered()),this, SLOT(changeIP()));
}
void MainWindow::changeIP()
{
    QDialog *getip = new QDialog(this);
    QVBoxLayout *iplayout = new QVBoxLayout();
    ip = new QLineEdit();
    QPushButton *OKButton = new QPushButton("OK");
    iplayout->addWidget(ip);
    iplayout->addWidget(OKButton);
    getip->setLayout(iplayout);
    getip->show();
    connect(OKButton,SIGNAL(clicked()),this, SLOT(updateIP()));
    connect(OKButton, SIGNAL(clicked()), getip, SLOT(accept()));
}
void MainWindow::updateIP()
{
    serv_ip = ip->text();
}

void MainWindow::next_token()
{
    char *reply = (char*)malloc(200*sizeof(char));
    if(communicate("csm\nend\n\0", reply, serv_ip.toStdString().c_str()) == EXIT_FAILURE)
    {
        QDialog *disp= new QDialog();
        QVBoxLayout *dialoglayout= new QVBoxLayout();
        QLabel *info = new QLabel("Connection to server failed.");
        connect(disp,SIGNAL(rejected()),this,SLOT(close()));
        dialoglayout->addWidget(info);
        disp->setLayout(dialoglayout);
        disp->show();
    }
    else
    {
        printf("at the else part\n");
        if(reply[0]==0 ||(reply[0]=='r' && reply[1]=='e' && reply[2]=='s' && reply[3]=='e' && reply[4]=='n' && reply[5]=='d'))
        {
            ui->textEdit_4->setText("No jobs to be serviced right now.");
        }
        else
        {
            accept_token(reply);
        }
    }

}

int MainWindow::accept_token(char *msg)
{
    char message[100];
    strcpy(message, msg);
    char* tok = strtok(message, " \n");
    Token newJob;
    tok = strtok(NULL, " \n");
    newJob.token_id = atoi(tok);
    tok = strtok(NULL, " \n");
    snprintf(newJob.name, strlen(tok)+1, "%s\0", tok);
    tok = strtok(NULL, " \n");
    newJob.job_type = atoi(tok);

    QString messages= QString::number(newJob.token_id) + "\nNAME: " + newJob.name + "\nTYPE: " + QString::number(newJob.job_type) + "\n TIME: " + QString::number(newJob.time);
    ui->textEdit_4->setText(messages);
}
MainWindow::~MainWindow()
{
    delete ui;
}
