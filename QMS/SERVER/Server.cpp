/* TESTED OK */

#include <iostream>
#include <cstdlib>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#define PORT 5000
#include "Token.cpp"
#include "queue.cpp"

using namespace std;

class Server
{

	public:
	int job_type_count;
	std::string server_ip;
	int port;
	int client_count;
	int *estimated_time;
	int token_count;
	Queue *job_queue;
	//calc ratio related variables
	int total_count;
	int temporary_count;
	int current_type;
	int exhaust;	

	Server(int no_of_types, int cli_count, int *est_time);
	int system_setup();
	int run_server();
	int accept_token(char *message);
	const char* allot_job();
	int find_queue();
};
Server::Server(int no_of_types, int cli_count, int *est_time)
{
	job_type_count = no_of_types;
	client_count = cli_count;
	token_count = 0;
	job_queue = new Queue[job_type_count];
	estimated_time = est_time;
	total_count = 0;
	for(int i=0; i<job_type_count; i++)
	{
		total_count = total_count>estimated_time[i]?total_count:estimated_time[i];
	}
	temporary_count = total_count;
	current_type = 0;
	exhaust = job_type_count;
}
int Server::run_server()
{
	
    	int listenfd = 0, connfd = 0;
    	struct sockaddr_in serv_addr; 

    	char sendBuff[1025];
    	char recvBuff[1025];

    	listenfd = socket(AF_INET, SOCK_STREAM, 0);
    	memset(&serv_addr, '0', sizeof(serv_addr));
    	memset(sendBuff, '0', sizeof(sendBuff)); 
    	memset(recvBuff, '0', sizeof(recvBuff));

	serv_addr.sin_family = AF_INET;
    	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    	serv_addr.sin_port = htons(PORT); 

    	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 
	
    	listen(listenfd, 10); 
    	printf("Server setup complete. Ready to connect.\n");
    	while(1)
    	{
        	connfd = accept(listenfd, (struct sockaddr*)NULL, NULL); 
   		int err = read(connfd, recvBuff, (sizeof(recvBuff)-1));
		recvBuff[err] = 0;
		char messageBuff[err+2];
		printf("MESSAGE IS:  %s\n\n", recvBuff);
		strcpy(messageBuff, recvBuff);
		char *token = strtok(recvBuff, " \n");
		if(strcmp(token, "tgm")==0)
		{
			if(accept_token(messageBuff) == EXIT_FAILURE)
				printf("ERROR IN TOKEN ACCEPTANCE");
			snprintf(sendBuff, sizeof(sendBuff), "ack\nend\n\r");
		}
		else if(strcmp(token, "csm")==0)
		{
			snprintf(sendBuff, sizeof(sendBuff), "%s\r\n", allot_job());
			printf("REPLY IS:\t%s", sendBuff);
		}
		write(connfd, sendBuff, strlen(sendBuff)); 
		sleep(1);		
        	close(connfd);
    	}	
}
int Server::accept_token(char *msg)
{
	char message[100];
	strcpy(message, msg);
	cout<<"OK"<<endl;
	char* tok = strtok(message, " \n");
	cout<<"OK"<<endl;
	Token newJob;
	tok = strtok(NULL, " \n");
	newJob.token_id = atoi(tok);
	cout<<"OK"<<endl;
	tok = strtok(NULL, " \n");
	snprintf(newJob.name, strlen(tok)+1, "%s\0", tok);
	cout<<"OK"<<endl;
	tok = strtok(NULL, " \n");
	newJob.job_type = atoi(tok);
	newJob.time = estimated_time[newJob.job_type];
	cout<<"OK"<<endl;
	
	
	if(job_queue[newJob.job_type].enque(newJob) == EXIT_SUCCESS)
	{
	    token_count++;
	    return EXIT_SUCCESS;
	}
	else
	{
	    cout<<"Error inserting token"<<endl;
	    return EXIT_FAILURE;
	}
}
int Server::find_queue()
{
	if(temporary_count > 0)
	{
		temporary_count = temporary_count - estimated_time[current_type];
		return current_type;
	}
	else
	{
		temporary_count = total_count;
		current_type = (current_type+1)%job_type_count;
		temporary_count = temporary_count - estimated_time[current_type];
		return current_type;
	}
}
const char* Server::allot_job()
{
	//code to find the new queue_no
//	while(1)
//	{	
		int queue_no= find_queue();
		Token *newJob = (Token *)malloc(sizeof(Token)); 
		string result;
		if(job_queue[queue_no].deque(newJob) == EXIT_SUCCESS)
		{
			exhaust = job_type_count;
			result = "server\n";
			char *temp = (char*)calloc(100,sizeof(char));
			sprintf(temp, "%d", newJob->token_id);
			result = result + string(temp) + "\n";
			result = result + newJob->name + "\n";
			sprintf(temp, "%d", newJob->job_type);
			result = result + string(temp) + "\n";
			sprintf(temp, "%d", newJob->time);
			result = result + string(temp) + "\n";
			result = result + "end\n";
//			break;
		}
		else
		{
//			if(exhaust>0)
//			{
//				exhaust--;
//				current_type++;
//				continue;
//			}
//			else
//			{	
				result = "resend\n";
//				break;
//			}
		}
	}	
	char *retres = (char*)malloc(100*sizeof(char));
	return result.c_str();
}
	
int main()
{
	int n,m;
	int *times;	
	cout<<"Number of types of jobs (n):\t";
	cin>>n;
	cout<<"Number of client service machines connected (m):\t";
	cin>>m;
	times = (int*)malloc(n*sizeof(int));
	for(int i=0; i<n; i++)
	{
		cout<<"Estimated time for job "<<i<<":\t";
		cin>>times[i];
	}

	Server serv(n,m,times);
	serv.run_server();
	cout<<"Server terminated. Goodbye."<<endl;
}




