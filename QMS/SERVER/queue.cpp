/* TESTED OK */

//#include <iostream>
//#include <stdlib.h>
//#include <string.h>
//#include "Token.cpp"
using namespace std;
class Node
{
    public:
	Token data;
	Node *link;
	Node();
	Node(Token d);
};
Node::Node()
{
    data = Token();
    link = NULL;
}
Node::Node(Token d)
{
    data = d;
    link = NULL;
}

class Queue
{
    public:
	Node *head;
	Queue();
	int enque(Token d);
	int deque(Token *t);
	void printIds();
};
Queue::Queue()
{
    this->head = NULL;
}
void Queue::printIds()
{
    Node *p = head;
    while(p!=NULL)
    {
	cout<<p->data.token_id<<"|"<<p->data.name<<"|||";
	p = p->link;
    }
    cout<<endl;
    return;
}
int Queue::enque(Token d)
{
    Node *temp = new Node(d);
    if(this->head == NULL)
    {
	head = temp;
	return EXIT_SUCCESS;
    }
    
    Node *p = head;
    while(p->link!=NULL)
	p=p->link;
    p->link = temp;
    return EXIT_SUCCESS;
}

int Queue::deque(Token *t)
{
    if(head == NULL)
	return EXIT_FAILURE;
    Token d = head->data;
    Node *f = head;
    head = head->link;
    free(f);
    *t = d;
    return EXIT_SUCCESS;
}